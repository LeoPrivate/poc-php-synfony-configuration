<VirtualHost *:80>
        ServerName symfony.conf
        DocumentRoot /var/www/symfony/current/public

        LogLevel warn
        ErrorLog /var/log/apache2/symfony_error.log
        CustomLog /var/log/apache2/symfony_access.log combined

            DirectoryIndex index.php
            RewriteEngine on

        <Directory /var/www/symfony/current/public>
            Options FollowSymLinks MultiViews
            AllowOverride All
            Require all granted
        </Directory>

</VirtualHost>

